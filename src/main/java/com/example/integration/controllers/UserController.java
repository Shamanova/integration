package com.example.integration.controllers;

import com.example.integration.dao.UserDao;
import com.example.integration.dto.UsersDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserDao userDao;

    public UserController(UserDao userDao) {
        this.userDao = userDao;
    }

    @GetMapping("/{lastName}")
    public UsersDto getUsersByLastName(@PathVariable String lastName) {
        return userDao.getUsersDataByLastName(lastName);
    }
}
