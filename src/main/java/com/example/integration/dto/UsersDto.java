package com.example.integration.dto;

import java.util.ArrayList;
import java.util.List;

public class UsersDto {
    private final List<UserDto> users = new ArrayList<>();

    public void addUser(UserDto userDto) {
        users.add(userDto);
    }

    public List<UserDto> getUsers() {
        return users;
    }
}
