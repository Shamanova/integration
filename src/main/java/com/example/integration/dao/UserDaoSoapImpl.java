package com.example.integration.dao;


import com.example.integration.configurations.WebServiceProperties;
import com.example.integration.dto.UserDto;
import com.example.integration.dto.UsersDto;
import com.example.integration.wsdl.UploadByLastName;
import com.example.integration.wsdl.UploadByLastNameResponse;
import com.example.integration.wsdl.User;
import com.example.integration.wsdl.Users;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class UserDaoSoapImpl extends WebServiceGatewaySupport implements UserDao {
    private final WebServiceProperties properties;

    public UserDaoSoapImpl(WebServiceProperties properties) {
        this.properties = properties;
    }

    @Override
    public UsersDto getUsersDataByLastName(String userLastName) {
        UploadByLastName request = new UploadByLastName();
        request.setUserLastName(userLastName);

        UploadByLastNameResponse response = (UploadByLastNameResponse) getWebServiceTemplate()
                .marshalSendAndReceive(
                        properties.wsPath(),
                        request,
                        new SoapActionCallback("http://myIntegration/getUsersByLastName")
                );
        return createUsersFromResponse(response.getReturn());
    }

    private UsersDto createUsersFromResponse(Users responseUsers) {
        UsersDto usersDto = new UsersDto();
        responseUsers.getUsers()
                .stream()
                .map(this::createUserFromResponseUser)
                .forEach(usersDto::addUser);
        return usersDto;
    }

    private UserDto createUserFromResponseUser(User responseUser) {
        return new UserDto(
                responseUser.getName(),
                responseUser.getLastname(),
                responseUser.getPhone(),
                responseUser.getAddress()
        );
    }
}
