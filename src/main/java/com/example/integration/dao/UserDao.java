package com.example.integration.dao;

import com.example.integration.dto.UsersDto;

public interface UserDao {

    UsersDto getUsersDataByLastName(String userLastName);

}
