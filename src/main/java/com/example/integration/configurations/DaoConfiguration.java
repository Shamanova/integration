package com.example.integration.configurations;

import com.example.integration.dao.UserDao;
import com.example.integration.dao.UserDaoSoapImpl;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
@EnableConfigurationProperties(value = WebServiceProperties.class)
public class DaoConfiguration {
    private final Jaxb2Marshaller marshaller;
    private final WebServiceProperties webServiceProperties;

    public DaoConfiguration(Jaxb2Marshaller marshaller, WebServiceProperties webServiceProperties) {
        this.marshaller = marshaller;
        this.webServiceProperties = webServiceProperties;
    }

    @Bean
    public UserDao userDao() {
        UserDaoSoapImpl client = new UserDaoSoapImpl(webServiceProperties);
        client.setDefaultUri(webServiceProperties.getHost());
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}
