package com.example.integration.configurations;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@ConfigurationProperties(prefix = "service")
@ConstructorBinding
public class WebServiceProperties {
    private final String host;
    private final String name;

    public WebServiceProperties(String host, String name) {
        this.host = host;
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public String getName() {
        return name;
    }

    public String wsPath() {
        return host+"/"+name;
    }
}
